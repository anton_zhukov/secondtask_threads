package com.dev.secondtask.entity;

import org.apache.log4j.Logger;

import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

public class CashBox extends Thread {

    private final static Logger LOG = Logger.getLogger(CashBox.class);

    private int cashBoxId;
    private AtomicBoolean isActive = new AtomicBoolean(false);
    private ConsumerQueue queue;
    private Eatery eatery = Eatery.getInstance();

    public CashBox(int id) {
        queue = new ConsumerQueue(id);
        this.cashBoxId = id;
    }

    //Method for adding queue with consumers to cashbox
    public void addQueue(Queue<Consumer> queue) {
        this.queue.addAll(queue);
    }

    // CashDesk condition: open
    public void openBox() {
        isActive.set(true);
    }

    // CashDesk condition: open
    public void closeBox() {
        isActive.set(false);
        LOG.info("Close cache box with id #" + cashBoxId);
    }

    private void waiting() {
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {
            LOG.error("InterruptedException", e);
        }
    }

    @Override
    public void run() {
        eatery.gateAwait();
        while (isActive.get()) {
            waiting();
            Lock lock = eatery.getLockForQueue(queue);
            try {
                lock.lock();
                if (!queue.isEmpty()) {
                    Consumer consumer = queue.poll();
                    consumer.makePurchase();
                } else {
                    closeBox();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    public boolean isActive() {
        return isActive.get();
    }

    public ConsumerQueue getQueue() {
        return queue;
    }
}
