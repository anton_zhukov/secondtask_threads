package com.dev.secondtask.entity;


import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Eatery {

    private final Logger LOG = Logger.getLogger(Eatery.class);
    private final CountDownLatch GATE = new CountDownLatch(1);
    private static final Lock LOCK = new ReentrantLock();
    public static final int CAPACITY = 3;

    private static Eatery instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean();
    private List<CashBox> cashBoxes = new ArrayList<>(CAPACITY);
    private Map<ConsumerQueue, Lock> lockHolder = new HashMap<>();

    private Eatery() {

    }

    public static Eatery getInstance() {
        if (!instanceCreated.get()) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new Eatery();
                    instanceCreated = new AtomicBoolean(true);
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public void openEatery() {
        for (CashBox cashBox : cashBoxes) {
            cashBox.openBox();
            lockHolder.put(cashBox.getQueue(), new ReentrantLock());
            for (Consumer consumer : cashBox.getQueue()) {
                consumer.setCurrentQueue(cashBox.getQueue());
                consumer.start();
            }
            cashBox.start();
        }
        GATE.countDown();
    }


    public void addCashBoxes(Queue<CashBox> cashBoxes) {
        this.cashBoxes.addAll(cashBoxes);
    }

    public List<CashBox> getCashBoxes() {
        return cashBoxes;
    }

    public Lock getLockForQueue(ConsumerQueue queue) {
        return lockHolder.get(queue);
    }

    public void gateAwait() {
        try {
            GATE.await();
        } catch (InterruptedException e) {
            LOG.error("InterruptedException", e);
        }
    }
}
