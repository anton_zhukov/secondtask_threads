package com.dev.secondtask.entity;

import org.apache.log4j.Logger;


import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Consumer extends Thread {

    private static final Logger LOG = Logger.getLogger(Consumer.class);

    private AtomicBoolean isInQueue = new AtomicBoolean(true);
    private Eatery eatery = Eatery.getInstance();
    private ConsumerQueue currentQueue;
    private static final Lock CHANGE_LOCK = new ReentrantLock();

    //Method for making purchase by consumer
    public void makePurchase() {
        isInQueue.set(false);
        LOG.info("Consumer # " + getId() + " wants to buy cheeseburger and coke!");
        LOG.info("Consumer # " + getId() + " bought it!");
    }

    @Override
    public void run() {
        eatery.gateAwait();
        while (isInQueue.get()) {
            try {
                Consumer.CHANGE_LOCK.lock();
                ConsumerQueue newQueue = chooseQueue();
                if (currentQueue.getId() != newQueue.getId()) {
                    Lock currentQueueLock = eatery.getLockForQueue(currentQueue);
                    Lock newQueueLock = eatery.getLockForQueue(newQueue);
                    currentQueueLock.lock();
                    newQueueLock.lock();
                    try {
                        if (new Random().nextInt(2) == 1) {
                            takeOtherConsumerQueue(newQueue);
                        } else {
                            exchangeConsumerQueue(newQueue);
                        }
                    } finally {
                        currentQueueLock.unlock();
                        newQueueLock.unlock();
                    }
                }
            } finally {
                Consumer.CHANGE_LOCK.unlock();
            }
        }
    }


    //Method for choosing desired queue
    private ConsumerQueue chooseQueue() {
        ConsumerQueue result = currentQueue;
        for (CashBox cashBox : eatery.getCashBoxes()) {
            if (cashBox.isActive()) {
                if (cashBox.getQueue().size() < currentQueue.size()) {
                    result = cashBox.getQueue();
                }
            }
        }
        return result;
    }

    //Method for taking desired queue
    private void takeOtherConsumerQueue(ConsumerQueue newQueue) {
        if (!currentQueue.isEmpty() && !newQueue.isEmpty()) {
            LOG.info("Consumer # " + getId() + " from queue # " + currentQueue.getId()
                    + " wants to move to the queue # " + newQueue.getId());
            currentQueue.remove(this);
            newQueue.addLast(this);
            currentQueue = newQueue;
            LOG.info("Consumer # " + getId() + " moved to the queue # " + newQueue.getId());
        }
    }

    //Method for exchanging queues with another customer
    private void exchangeConsumerQueue(ConsumerQueue newQueue) {
        if (!currentQueue.isEmpty() && !newQueue.isEmpty() && currentQueue.indexOf(this) > -1) {
            int indexCurrent = currentQueue.indexOf(this);
            int randomIndex = new Random().nextInt(100) % newQueue.size();
            Consumer consumerExchange = newQueue.get(randomIndex);
            LOG.info("Consumer # " + getId() + " from queue # " + currentQueue.getId()
                    + " wants to exchange queues with Consumer # " + consumerExchange.getId() + " from queue # "
                    + newQueue.getId());

            currentQueue.remove(indexCurrent);
            currentQueue.add(indexCurrent, consumerExchange);

            newQueue.remove(randomIndex);
            newQueue.add(randomIndex, this);

            ConsumerQueue tempQueue = currentQueue;
            currentQueue = newQueue;
            consumerExchange.setCurrentQueue(tempQueue);

            LOG.info("Consumer # " + getId() + " exchanged queues with Consumer # "
                    + consumerExchange.getId());
        }
    }

    public void setCurrentQueue(ConsumerQueue currentQueue) {
        this.currentQueue = currentQueue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Consumer)) return false;

        Consumer consumer = (Consumer) o;

        return getId() == consumer.getId();
    }
}
