package com.dev.secondtask.entity;

import java.util.LinkedList;

public class ConsumerQueue extends LinkedList<Consumer> {

    private int id;

    public ConsumerQueue(int id) {
        super();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ConsumerQueue consumers = (ConsumerQueue) o;

        return id == consumers.id;
    }

    @Override
    public int hashCode() {
        return 31 * id;
    }
}
