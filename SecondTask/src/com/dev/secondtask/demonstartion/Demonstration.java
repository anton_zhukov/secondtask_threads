package com.dev.secondtask.demonstartion;

import com.dev.secondtask.creation.CreationCashBoxesLine;
import com.dev.secondtask.creation.CreationConsumers;
import com.dev.secondtask.entity.CashBox;
import com.dev.secondtask.entity.Eatery;

import java.util.Queue;

public class Demonstration {

    public static void demonstrate() {
        Eatery eatery = Eatery.getInstance();
        Queue<CashBox> cashBoxes = CreationCashBoxesLine.createCashBoxesLine();

        int consumerNumber = 1;
        for (CashBox cashBox : cashBoxes) {
            cashBox.addQueue(CreationConsumers.createConsumers(consumerNumber));
            consumerNumber += 3;
        }
        eatery.addCashBoxes(cashBoxes);
        eatery.openEatery();
    }
}
