package com.dev.secondtask.creation;

import com.dev.secondtask.entity.Consumer;

import java.util.ArrayDeque;
import java.util.Queue;

public class CreationConsumers {

    public static Queue<Consumer> createConsumers(int size) {
        Queue<Consumer> consumers = new ArrayDeque<>();
        for (int i = 0; i < size; i++) {
            consumers.add(new Consumer());
        }
        return consumers;
    }
}
