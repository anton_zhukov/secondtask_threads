package com.dev.secondtask.creation;

import com.dev.secondtask.entity.CashBox;
import com.dev.secondtask.entity.Eatery;

import java.util.ArrayDeque;
import java.util.Queue;

public class CreationCashBoxesLine {

    public static Queue<CashBox> createCashBoxesLine() {
        Queue<CashBox> cashBoxes = new ArrayDeque<>(Eatery.CAPACITY);
        for (int i = 1; i <= Eatery.CAPACITY; i++) {
            cashBoxes.add(new CashBox(i));
        }
        return cashBoxes;
    }
}
